
export function teamSection() {
    const section = document.querySelector('.team.section');
    const sectionDivs = section.querySelectorAll('.team.section > div');
    sectionDivs.forEach(div => {
        div.remove();
    });

    const teams = ['Маркетинг', 'Продажи', 'Финансы', 'Дизайн', 'Бекенд', 'Фронтенд'];
    for (const team of teams) {
        const img = `
        <img src="img/people/Oval-1.jpg" alt="">
        `;
        const divItem = `
        <div class="team__description flex">
            <p class="team__side">${team}</p>
            <div class="team__members flex">
                
            </div>
        </div>
        `;
        section.insertAdjacentHTML('beforeend', divItem);
    } 
}
export function mouse(element) {
    element.addEventListener('mouseenter', () => {
        element.style.opacity = '70%';
        element.querySelector('p').style.cursor = 'pointer';
    });
    element.addEventListener('mouseleave', () => {
        element.style.opacity = '100%';
    });
}

/////////////////////////////Does not work
export const divActiveArray = [];

const sectionDivsAppended = document.querySelectorAll('.team.section > div');
    sectionDivsAppended.forEach(div => {
        div.addEventListener('click', () => {
            const divActive = div.querySelector('p').textContent;
            divActiveArray.push(divActive);
        });
    });
////////////////////////////////////
export function goToTeam() {
    const sectionDivsAppended = document.querySelectorAll('.team.section > div');
    sectionDivsAppended.forEach(div => {
        mouse(div);
        div.addEventListener('click', () => {
            const divActive = div.querySelector('p').textContent;//////////////
            localStorage.setItem('teamClicked', JSON.stringify(divActive));
            window.location.href = 'team.html';
        });       
    });
}

export function userProfile() {
    const profileDiv = document.querySelector('.user.user__section.flex');
    const userName = profileDiv.querySelector('div > p.user__name');
    const userRole = profileDiv.querySelector('div > p.user__role');
   
    const login = JSON.parse(localStorage.getItem('login'));
    userName.textContent = login.name;
    userRole.textContent = login.team;
    profileDiv.addEventListener('click', () => {
        window.location.href = 'profile-settings.html';
    });

}




