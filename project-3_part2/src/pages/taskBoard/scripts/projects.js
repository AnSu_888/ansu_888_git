import { taskColumns } from "./appendTask";
import { populateFromLocalStorage, removeInitial } from "./populate";

function taskClickRedirect() {
    const taskBlocks = document.querySelectorAll('.task.task__block');
    taskBlocks.forEach(task => {
        task.addEventListener('click', () => {
            window.location.href = 'task-description.html';
        });
    });
}
export function projects() {
    const pjList = document.querySelectorAll('.projects__list > li > a');
    
    pjList.forEach(li => {
        const clicked = JSON.parse(localStorage.getItem('pjClicked'));
        if (clicked) {
            document.querySelector('h1').textContent = clicked;
        }
        else {
            document.querySelector('h1').textContent = pjList[0].textContent; 
        }
        
        if (li.textContent === document.querySelector('h1').textContent) {
            li.style.color = '#F3F4B2';
            window.addEventListener('load', () => {
                populateFromLocalStorage(taskColumns);
            });
        }

        li.addEventListener('click', () => {
            removeInitial(taskColumns);
            setTimeout(populateFromLocalStorage, 50, taskColumns);
            
            for (let count = 0; count < pjList.length; count++) {
                pjList[count].removeAttribute('style');
            }
            li.style.color = '#F3F4B2';
            document.querySelector('h1').textContent = li.textContent;

            setTimeout(taskClickRedirect, 60);
        });
    });
    setTimeout(taskClickRedirect, 60); 
}
