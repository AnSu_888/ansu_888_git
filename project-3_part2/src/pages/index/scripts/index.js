import '../../../css/style.scss';
import { validateEmail, validateName, checkUserExistence, errorDivRemove, checkUserNonExistence, wrongPassword } from './validation';

const userObject = {};
//toastr.info('Page Loaded!');

const pageRegistration = document.querySelector('.form--registration');
const pageLogin = document.querySelector('.form--login');
const pageSetup = document.querySelector('.form--setup');

const formRegistration = pageRegistration.querySelector('.form--registration > form');
const registrationLink = pageLogin.querySelector('.additional__info > a');
const regEmail = formRegistration.querySelector('#email');
const regPass = formRegistration.querySelector('#pass');
const regName = formRegistration.querySelector('#user');

const formLogin = pageLogin.querySelector('.form--login > form');
const loginLink = pageRegistration.querySelector('.additional__info > a');
const loginEmail = formLogin.querySelector('#email');
const loginPass = formLogin.querySelector('#pass');

const formTeamSelect = pageSetup.querySelector('form');
const radios = formTeamSelect.querySelectorAll('input');

loginLink.addEventListener('click', () => {
    pageRegistration.style.display = 'none';
    pageLogin.style.display = 'block';
});
registrationLink.addEventListener('click', () => {
    pageLogin.style.display = 'none';
    pageRegistration.style.display = 'block';
});

const key = 'credentials';
const existing = JSON.parse(localStorage.getItem(key)) || [];

formRegistration.addEventListener('submit', (event) => {
    event.preventDefault();
    
    userObject.name = regName.value;
    userObject.email = regEmail.value.toLowerCase();
    userObject.password = regPass.value;
    
    errorDivRemove(formRegistration, 'invalidName', regName);
    errorDivRemove(formRegistration, 'invalidEmail', regEmail);
    checkUserExistence(existing, regEmail); 
    validateEmail(regEmail);
    validateName(regName);

    pageRegistration.style.display = 'none';
    pageSetup.style.display = 'block';
});

formTeamSelect.addEventListener('submit', (event) => {
    event.preventDefault();
    radios.forEach(radio => {
        if (radio.checked) {
            userObject.team = radio.closest('label').textContent.trim();
            existing.push(userObject);
            localStorage.setItem(key, JSON.stringify(existing));

            localStorage.setItem('login', JSON.stringify(userObject));
            window.location.href = 'task-board.html';
        }
    });
    
});
formLogin.addEventListener('submit', (event) => {
    event.preventDefault();
    const arrayOfUsers = JSON.parse(localStorage.getItem('credentials'));
    errorDivRemove(formLogin, 'wrongPass', loginPass);
    errorDivRemove(formLogin, 'noUser', loginEmail);
    checkUserNonExistence(arrayOfUsers, loginEmail);
    wrongPassword(arrayOfUsers, loginPass);

    arrayOfUsers.forEach(user => {
        if (loginEmail.value === user.email && loginPass.value === user.password) {
            localStorage.setItem('login', JSON.stringify(user));
            window.location.href = 'task-board.html';
        }
    });
});



