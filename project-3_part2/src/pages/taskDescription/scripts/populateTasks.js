import { taskCreate2 } from "./taskCreate2";

export function clearTasks(list) {
    list.forEach(task => {
        task.remove();
    });
}
export function populateTasks(key, column) {
    if (key) {
        key.forEach(object => {       
            const titles = column.querySelectorAll('.item__title');
            titles.forEach(title => {
                if (object.section === title.textContent) {
                    title.closest('div').parentNode.insertAdjacentHTML('beforeend', taskCreate2(object));
                    }
                });   
        }); 
    }
}