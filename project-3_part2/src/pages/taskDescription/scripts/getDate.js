export function pushTaskDate() {
    const now1 = new Date();
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric',
    };
    return now1.toLocaleString('en-US', options);
}

export function getTaskDate(string) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];
    
    const now = new Date();
    const year = now.getFullYear();
    const month = monthNames[now.getMonth()];
    const date = now.getDate();
    
    if (string.includes(month) && string.includes(date) && string.includes(year)) {
        const today = 'today at' + string.slice(string.lastIndexOf(',') + 1);
        return today;
    }
    else {
      return `on ${string}`;
    }
}

export function pushShortDate() {
    const now3 = new Date();
    const year = now3.getFullYear();
    const month = now3.getMonth() + 1;
    const date = now3.getDate();
    const hours = now3.getHours();
    const minutes = now3.getMinutes();
    const span1 = `${date}.${month}.${year}`;
    const span2 = `${hours}:${minutes}`;
    if (hours < 10) {
        return `${span1} 0${span2}`;
    }
    else {
        return `${span1} ${span2}`;
    }   
}