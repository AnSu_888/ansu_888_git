import '../../../css/style.scss';
import { getTaskDate } from './getDate';
import { addComments, clearComments } from './comments';
import { projects } from './projects';
import { populateComments } from './populateComments';
import {populateTasks} from './populateTasks';
import { clearTasks } from './populateTasks';
import { pushShortDate } from './getDate';
import { teamSection, goToTeam, userProfile } from '../../taskBoard/scripts/teamSection';
import { countTaskInMenu } from '../../team/scripts/countTasks';
teamSection();
goToTeam();
userProfile();
countTaskInMenu();


const taskColumn = document.querySelector('.tasks__column');
const clearTaskList = taskColumn.querySelectorAll('a:not(.item__btn)');

const tasKDescrCard = document.querySelector('.task__desctiption.task__desctiption__card');
tasKDescrCard.style.display = 'none';
const cardTitle = tasKDescrCard.querySelector('.item__title');
const author = tasKDescrCard.querySelector('.author__list');
const execuror = tasKDescrCard.querySelector('.executor__name');
const cardTag = tasKDescrCard.querySelector('.task__label.development');
const dueon = tasKDescrCard.querySelector('.dueon__date.legend');

const cardDescr = tasKDescrCard.querySelector('.task__desctiption__textarea > p');
const titleStyle = document.querySelectorAll('.task__title1 > b');

projects();
clearTasks(clearTaskList);
clearComments();

const tasks = JSON.parse(localStorage.getItem('tasks'));
populateTasks(tasks, taskColumn);

const taskBlock = taskColumn.querySelectorAll('.task.task__block');
taskBlock.forEach(task => {
    const checkBox = task.querySelector('.checkbox');
        checkBox.addEventListener('click', (e) => {
            e.stopPropagation();
            checkBox.closest('a').classList.toggle('task--finished');

            const finTask = {};
            finTask.title = checkBox.closest('a').querySelector('.task__title1').textContent;
            finTask.author = JSON.parse(localStorage.getItem('login')).name;
            finTask.date = pushShortDate();
            
            const key = 'finishedTasks';
            const finished = JSON.parse(localStorage.getItem(key)) || [];
            
            function preventDuplicates() {
                finished.forEach(fin => {
                    if (fin.title === checkBox.closest('a').querySelector('.task__title1').textContent) {
                        throw new Error('Duplicate');
                    }
                });
            }
            preventDuplicates();
            finished.push(finTask);
            localStorage.setItem(key, JSON.stringify(finished)); 
        });

    task.addEventListener('click', () => {
        clearComments();

        if (task.classList.contains('task--active')) {
            task.classList.remove('task--active');
            tasKDescrCard.style.display = 'none';
        }
        else {
            for (let count = 0; count < taskBlock.length; count++) {
                taskBlock[count].classList.remove('task--active');
            }
            task.classList.add('task--active');
            tasKDescrCard.style.display = 'block';
            
            const taskHeading = task.querySelector('.task__title1 > b').textContent;
            const getLogin = JSON.parse(localStorage.getItem('login'));

            tasks.forEach(object => {
                if (taskHeading === object.title) {
                    cardTitle.textContent = object.title;
                    cardDescr.textContent = object.description;
                    execuror.textContent = object.contractor;
                    author.textContent = `Added by ${getLogin.name} ${getTaskDate(object.date)}`;
                    dueon.textContent = object.deadline;
                    cardTag.textContent = object.tag;   
                }
            });
        }
        populateComments(); 
    });
});
addComments();


titleStyle.forEach(title => {
    if (title) {
        title.style.color = 'black';
    }
});

const navItems2 = document.querySelectorAll('.tab__navigation > li');
navItems2[0].addEventListener('click', () => {
    window.location.href = 'task-board.html';
});
navItems2[2].addEventListener('click', () => {
    window.location.href = 'calendar.html';
});


