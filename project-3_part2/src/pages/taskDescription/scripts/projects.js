export function projects() {
    const pjList = document.querySelectorAll('.projects__list > li > a');
    console.log(pjList);
    pjList.forEach(li => {
        li.addEventListener('click', () => {
            const pjClicked = li.textContent;
            localStorage.setItem('pjClicked', JSON.stringify(pjClicked));
            window.location.href = 'task-board.html';
        });
    });
}

