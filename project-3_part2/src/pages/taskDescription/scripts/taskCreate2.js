export function taskCreate2(obj) {
    const taskHtml2 = `
    <a href="#" class="task task__block">
    <span class="checkbox">
        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">
            <path id="Path" d="M11.561.44a1.5,1.5,0,0,0-2.124,0L4.5,5.379,2.56,3.441A1.5,1.5,0,0,0,.439,5.562l3,3a1.5,1.5,0,0,0,2.121,0l6-6A1.5,1.5,0,0,0,11.561.44Z" fill="#b8b8b8" />
        </svg>
        <input type="checkbox">
    </span>
    <div>
        <p class="task__title1"><b>${obj.title}</b></p>
        <p class="task__description">${obj.description}</p>
        <div class="flex">
            <img src="img/userpic.jpg" class="task__executor" alt="">
            <p class="task__label development">${obj.tag}</p>
        </div>
    </div>					
</a>
    `;
    return taskHtml2;
}