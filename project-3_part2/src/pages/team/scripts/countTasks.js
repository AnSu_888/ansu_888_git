export function countTaskInMenu() {
    const tasks = JSON.parse(localStorage.getItem('tasks'));

    const tasksClosed = document.querySelector('.user__statistics.flex > .tasks.tasks--closed > .tasks__number');
    const tasksOpened = document.querySelector('.user__statistics.flex > .tasks.tasks--open > .tasks__number');
    window.addEventListener('load', () => {
        const profileName = document.querySelector('.user.user__section.flex > div > .user__name').textContent;
        const opened = tasks.filter(object => {
            return object.author === profileName && object.section !== 'Выполнено';
        });
        const closed = tasks.filter(object => {
            return object.author === profileName && object.section === 'Выполнено';
        });
        tasksClosed.textContent = closed.length;
        tasksOpened.textContent = opened.length;
    });
}