import '../../../css/style.scss';

import { teamSection, userProfile } from '../../taskBoard/scripts/teamSection';
import { mouse } from '../../taskBoard/scripts/teamSection';
import { cardListClear, activityRemove } from './clearFunc';
import { taskListClear } from './clearFunc';
import { populateCards, profileDisplay } from './displayFunc';
import { taskCount } from './displayFunc';
import { countTaskInMenu } from './countTasks';
import { projects } from '../../taskDescription/scripts/projects';
countTaskInMenu();


//import { divActiveArray } from '../../taskBoard/scripts/teamSection';//////Does not work
//console.log(divActiveArray[0]);

teamSection();
taskListClear();
cardListClear();
userProfile();
projects();

const cardSection = document.querySelector('.members.members__list');
const profile = document.querySelector('.profile.member__profile');

activityRemove();

profile.style.display = 'none';
const membersCount = cardSection.querySelector('.members__count');
membersCount.textContent = null;

const teamClicked = JSON.parse(localStorage.getItem('teamClicked'));
document.querySelector('h1').textContent = teamClicked;

const credentials = JSON.parse(localStorage.getItem('credentials'));
credentials.forEach(object => {
    if (object.team === teamClicked) {  
        cardListClear();
        
        setTimeout(populateCards, 30, object, cardSection, membersCount);
        setTimeout(taskCount, 35, cardSection);
        setTimeout(profileDisplay, 45/*, cardSection, profile*/);
    }
    else {
        cardListClear();
        const cardsAppended2 = cardSection.querySelectorAll('.members__card.flex');
        membersCount.textContent = cardsAppended2.length;
    }  
});

const sectionDivsAppended = document.querySelectorAll('.team.section > div');
sectionDivsAppended.forEach(div => {
    mouse(div);
    div.addEventListener('click', () => {
        document.querySelector('h1').textContent = div.querySelector('p').textContent;
        profile.style.display = 'none';
        const credentials = JSON.parse(localStorage.getItem('credentials'));
        credentials.forEach(object => {
            if (div.querySelector('p').textContent === object.team) {   
                cardListClear();
                setTimeout(populateCards, 30, object, cardSection, membersCount);
                setTimeout(taskCount, 35, cardSection);
                setTimeout(profileDisplay, 45/*, cardSection, profile*/);
            }
            else {
                cardListClear();
                const cardsAppended2 = cardSection.querySelectorAll('.members__card.flex');
                membersCount.textContent = cardsAppended2.length;
            }
        });
    });       
});


const home = document.querySelector('.menu__list > li');
home.addEventListener('click', () => {
    window.location.href = 'task-board.html';
});

