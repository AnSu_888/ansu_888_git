export function taskListClear() {
    const assigned = document.querySelectorAll('.max__container > .task.task__block');
    assigned.forEach(task => {
        task.remove();
    });
}

export function cardListClear() {
    const cards = document.querySelectorAll('.members__card.flex');
    cards.forEach(card => {
        card.remove();
    });
}

export function activityRemove() {
    const initialActivity = document.querySelectorAll('.activity__notification.flex');
    initialActivity.forEach(activity => {
        if (activity) {
            activity.remove();
        }
    });    
}