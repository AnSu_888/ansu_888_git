import { taskListClear, activityRemove } from "./clearFunc";

function appendAssignedTask(crd) {
    const maxContainer = document.querySelector('.max__container');
    const tasks6 = JSON.parse(localStorage.getItem('tasks')); 
    tasks6.forEach(object => {
        if (object.contractor === crd.querySelector('div > p').textContent) {
            const assignedHtml = `
            <a href="#" class="task task__block">
                <span class="checkbox">
                    <img src="img/check-icon.svg" alt="">
                </span>
                <div>
                    <p class=""><b>${object.title}</b></p>
                    <p class="task__description">${object.description}</p>
                    <div class="flex">
                        <p class="task__label development">${object.tag}</p>
                    </div>
                </div>					
            </a>
            `;
            maxContainer.insertAdjacentHTML('afterbegin', assignedHtml);

        }
        
    });
}

export function populateCards(obj, section, count) {
    const memberCard = `
    <div class="members__card flex">
        <img src="img/userpic-big.jpg" class="members__icon" alt="">
        <div>
            <p class="members__name">${obj.name}</p>
            <p class="members__role">${obj.team}</p>
        </div>

        <div class="members__tasks">
            <p class="tasks__count">285</p>
            <p class="plain__text">задач</p>
        </div>
    </div>
`; 
    section.insertAdjacentHTML('beforeend', memberCard);
    const cardsAppended = section.querySelectorAll('.members__card.flex');
    count.textContent = cardsAppended.length;
}

export function taskCount(section) {
    const tasks4 = JSON.parse(localStorage.getItem('tasks')); 
    const cardsAppended3 = section.querySelectorAll('.members__card.flex');
        cardsAppended3.forEach(card => {
            
            const filtered = tasks4.filter(object => {
                return object.author === card.querySelector('div > p').textContent;
            });
            card.querySelector('.tasks__count').textContent = filtered.length;
            if (filtered.length === 1) {
                card.querySelector('.plain__text').textContent = 'задача';
            }
            else if (filtered.length === 2 || filtered.length === 3 || filtered.length === 4) {
                card.querySelector('.plain__text').textContent = 'задачи';
            }
            else {
                card.querySelector('.plain__text').textContent = 'задач';
            }
    });
}

function countTaskCategories(selector, eventItem) {
    const tasks5 = JSON.parse(localStorage.getItem('tasks'));
    const appendedCardName = eventItem.querySelector('div > p').textContent;
    const opened = tasks5.filter(object => {
        return object.author === appendedCardName && object.section !== 'Выполнено';
    });
    const closed = tasks5.filter(object => {
        return object.author === appendedCardName && object.section === 'Выполнено';
    });
    const assigned = tasks5.filter(object => {
        return object.contractor === appendedCardName;
    });

    const count = document.querySelectorAll('.count');
    const text = document.querySelectorAll('.text');
    count[0].textContent = closed.length;
    count[1].textContent = opened.length;
    selector.querySelector('h2.subtitle > .count').textContent = assigned.length;
}

export function profileDisplay() {
    const profile2 = document.querySelector('.profile.member__profile');
    const cardsAppended4 = document.querySelectorAll('.members__card.flex');
    cardsAppended4.forEach(card => {
        card.addEventListener('click', () => {
            taskListClear();
            activityRemove();

            for (let count = 0; count < cardsAppended4.length; count++) {
                cardsAppended4[count].removeAttribute('style');
            }
            card.style.backgroundColor = '#E9E9D8';
            profile2.style.display = 'block';

            const profileName = document.querySelector('.profile__name');
            const profileRole = document.querySelector('.profile__role');

            profileName.textContent = card.querySelector('.members__name').textContent;
            profileRole.textContent = card.querySelector('.members__role').textContent;
            
            countTaskCategories(profile2, card);
            appendAssignedTask(card);

            const tasks7 = JSON.parse(localStorage.getItem('tasks'));
            const tasksByThisUser = tasks7.filter(task => {
                return task.author === profileName.textContent;
            });
           
            const finishedTasks = JSON.parse(localStorage.getItem('finishedTasks'));

            finishedTasks.forEach(task => {
                tasksByThisUser.forEach(taskTask => {
                    const activitySelector = document.querySelector('.activity.activity__block');
                    if (task.title === taskTask.title) {
                        const activityHtml = `
                        <div class="activity__notification flex">
                        <img src="img/activity.svg" class="activity__icon" alt="">
                            <p class="activity__text">Пользователь <a href="team.html">${task.author}</a> отметил как выполненную задачу <a href="task-board.html">${task.title}</a></p>
                            <p class="activity__time">
                                <span>${task.date.split(' ')[0]}</span>
                                <span>${task.date.split(' ')[1]}</span>
                            </p>
                            <br><br><br>
                        </div>
                        `;
                        activitySelector.insertAdjacentHTML('beforeend', activityHtml);
                    } 
                }); 
            });
        });
    });
}