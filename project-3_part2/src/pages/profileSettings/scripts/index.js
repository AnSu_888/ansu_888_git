import '../../../css/style.scss';
import { userProfile, teamSection, goToTeam } from '../../taskBoard/scripts/teamSection';
import { countTaskInMenu } from '../../team/scripts/countTasks';
import { projects } from '../../taskDescription/scripts/projects';
teamSection();
goToTeam();
userProfile();
countTaskInMenu();
projects();

const form = document.querySelector('.form.form--profile > form');
const name = form.querySelector('input#user');
const role = form.querySelector('input#role');
const about = form.querySelector('#about');

const cancelBtn = form.querySelector('.cancel__btn');
cancelBtn.setAttribute('type', 'reset');

const profileDiv = document.querySelector('.user.user__section.flex');
const userName = profileDiv.querySelector('div > p.user__name');
const userRole = profileDiv.querySelector('div > p.user__role');
name.value = userName.textContent;
role.value = userRole.textContent;

const teamInput = document.querySelector('.team__wrapp.gap--bottom');
const teamNames = document.querySelectorAll('.team__name');
const input = teamInput.querySelector('input');
teamNames.forEach(initial => {
initial.remove();
});

input.addEventListener('change', () => {
    const teamValue = `<div class="team__name">
    ${input.value} 
    <img src="img/icon/close.svg" class="close__icon" alt="">
    </div>
    `;
    teamInput.insertAdjacentHTML('afterbegin', teamValue);

    const teamName = document.querySelector('.team__name');
    const x = teamName.querySelector('.close__icon');
    x.addEventListener('click', (e) => {
        e.stopPropagation();
        x.closest('div').remove();
    });
    input.value = null;
});


const credentials = JSON.parse(localStorage.getItem('credentials'));

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const profileData = {};
    profileData.name = name.value;
    profileData.team = role.value;
    profileData.about = about.value;

    const teamNames2 = document.querySelectorAll('.team__name');
    profileData.additionalTeams = [];
    teamNames2.forEach(t => {
        profileData.additionalTeams.push(t.textContent.trim());
        });
    
    credentials.forEach(user => {
        if (user.name === userName.textContent) {
            localStorage.setItem('previousUserData', JSON.stringify(user));
            profileData.email = user.email;
            profileData.password = user.password;
            credentials.splice(credentials.indexOf(user), 1, profileData);
            localStorage.setItem('credentials', JSON.stringify(credentials));

            const login = JSON.parse(localStorage.getItem('login'));
            login.name = profileData.name;
            login.team = profileData.team;
            login.about = profileData.about;
            localStorage.setItem('login', JSON.stringify(login));
            setTimeout(userProfile, 100);
        }
    });
});

cancelBtn.addEventListener('click', () => {
    const profileDiv = document.querySelector('.user.user__section.flex');
    const userName = profileDiv.querySelector('div > p.user__name');

    const credentials = JSON.parse(localStorage.getItem('credentials'));

    credentials.forEach(user => {
        if (user.name === userName.textContent) {
            const previousData = JSON.parse(localStorage.getItem('previousUserData'));
            credentials.splice(credentials.indexOf(user), 1, previousData);
            localStorage.setItem('credentials', JSON.stringify(credentials));

            const login = JSON.parse(localStorage.getItem('login'));
            login.name = previousData.name;
            login.team = previousData.team;
            login.about = previousData.about;
            localStorage.setItem('login', JSON.stringify(login));
            setTimeout(userProfile, 100);
            function revert() {
                const userName = profileDiv.querySelector('div > p.user__name');
                const userRole = profileDiv.querySelector('div > p.user__role');
                name.value = userName.textContent;
                role.value = userRole.textContent;
            }
            setTimeout(revert, 110);
        }
    });
});
