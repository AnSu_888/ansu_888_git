/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 * 
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 * 
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 * 
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/
'use strict';
const fromEntries = (entries) => {
    function errorThrow(secondElem) {
        if (typeof(secondElem) !== 'number' && typeof(secondElem) !== 'string' && secondElem !== 'null' && !Array.isArray(secondElem)) {
            throw new Error('Value is not a number and not a string!');
        }
    }

    const obj = {};
    function objectCreate() {
        for (const [key, value] of entries) {
            try {
            errorThrow(value);
            } catch(error) {
                console.log(error.message);
            }
            obj[key] = value;
            
            if (Array.isArray(value)) {
                const obj2 = {};
                for (const [k, val] of value) {
                    obj2[k] = val;
                    obj[key] = obj2;
                }
            }
          }
    }
    objectCreate();
    return obj;
} 

console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]]));
console.log(fromEntries([['name', 'John'], ['age', 35]]));

