/**
 * Задача 1.
 *
 * Создайте функцию shallowMerge.
 * Функция обладает двумя параметрами, каждый из которых должен быть обычным JavaScript объектом.
 * Функция возвращает новый объект, который в себе объединяет свойства обоих объектов.
 * Свойства необходимо копировать лишь на один уровень каждого их объектов.
 *
 * Из объектов и обеих аргументах копируются только собственные свойства, включая недоступные для перечисления.
 *
 * Условия:
 * - Встроенный метод Object.create() использовать запрещено;
 * - При копировании любого свойства необходимо обязательно сохранить его дескрипторы;
 * - Свойства с одинаковыми именами нужно перезаписывать — приоритетом обладает объект из второго параметра.
 */

// Решение

const objectOfDescriptors = {};
const objectOfNonEnum = {};

const user = { firstName: 'Marcus', lastName: 'Kronenberg' };
const userData = { job: 'developer', country: 'Germany', lastName: 'Schmidt' };

Object.defineProperty(user, 'firstName', { writable: false });
Object.defineProperty(userData, 'job', { configurable: false });
Object.defineProperty(userData, 'country', { enumerable: false });



function getDescriptors(objName) {
    function enumerable() {
        Object.getOwnPropertyNames(objName).forEach(function(property) {
            if (Object.getOwnPropertyDescriptor(objName, property).enumerable === false) {               
                objectOfNonEnum[property] = Object.getOwnPropertyDescriptor(objName, property).value;
            }
        });
    }
    enumerable();
    
    Object.keys(objName).forEach(key => {
        const descriptors = Object.getOwnPropertyDescriptor(objName, key);
        for (const descrKey in descriptors) {
            if (descriptors[descrKey] === false) {
                objectOfDescriptors[key] = descrKey;
            } 
        }
    });
}

function shallowMerge(obj1, obj2) {
    getDescriptors(obj1); 
    getDescriptors(obj2);  
    
    const clone = Object.assign({}, obj1, obj2, objectOfNonEnum);
    for (const key in objectOfDescriptors) {
        function returnVal() {
            const obj = {};
            obj[objectOfDescriptors[key]] = false;
            return obj;
        }
       Object.defineProperty(clone, key, returnVal());  
    }

    return clone;
}
const result = shallowMerge(user, userData);



console.log(result);
for (const keys in objectOfNonEnum) {
    Object.defineProperty(result, keys, {enumerable: false});
}

console.log(Object.getOwnPropertyDescriptor(result, 'firstName').writable); // false
console.log(Object.getOwnPropertyDescriptor(result, 'job').configurable); // false
console.log(Object.getOwnPropertyDescriptor(result, 'country').enumerable); // false


