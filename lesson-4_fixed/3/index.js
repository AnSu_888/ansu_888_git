/*
 * Задача 3.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 * 
 * Пример 1: string -> 'Вот, что мне хотелось бы сказать на эту тему:'
 *         maxLength -> 21
 *         result -> 'Вот, что мне хотел...'
 * 
 * Пример 2: string -> 'Вот, что мне хотелось'
 *         maxLength -> 100
 *         result -> 'Вот, что мне хотелось'
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 * 
 */
const string = prompt('Введите строку, которую нужно сократить:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

function promptDisplay() {
    const maxLength = prompt('Введите максимальную длину строки:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
    if (isNaN(+maxLength)) {
        console.log('Введите число.');
        promptDisplay();
       } 
       else if (maxLength === null || maxLength === '' || string.length < maxLength) {
           console.log(string); 
       }
       else {
           const truncString = string.slice(0, maxLength - 3);
           console.log(`${truncString}...`); 
       }
}
promptDisplay();


/*

const string = prompt('Введите строку, которую нужно сократить:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
const maxLength = prompt('Введите максимальную длину строки:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

let maxLengthNumber = +maxLength;
while (/\D/.test(maxLengthNumber) && maxLengthNumber !== null) {
    console.log('Please enter a number!');
    maxLengthNumber = prompt('Введите максимальную длину строки:');
} 
if (maxLengthNumber === null || maxLengthNumber === '' || maxLength === null || maxLength === '' || string.length < maxLengthNumber) {
    console.log(string);   
} 
else {
    const truncString = string.slice(0, maxLengthNumber - 3);
    console.log(`${truncString}...`);    
}

*/




