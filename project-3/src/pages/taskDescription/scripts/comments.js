import { pushTaskDate } from "./getDate";
const commentSection = document.querySelector('.task__desctiption__comments');
const commentHistorySection = commentSection.querySelector('.comment__history');

export function clearComments() {
    const commentHistory2 = document.querySelectorAll('.commet.comment__block');
        commentHistory2.forEach(block => {
            block.remove();
        });
}


export function addComment(obj) {
    const commentHtml = `
    <div class="commet comment__block">
        <div class="commet__icon">
            <img src="img/userpic-big.jpg" alt="">
        </div>
        <div class="commet__text__wrapp">
            <p class="commet__name">${obj.name}<span class="commet__role"> из команды «${obj.team}»</span></p>
            <p class="commet__text">${obj.comment}</p>
            <p class="commet__date">${obj.date}</p>
        </div>
    </div>
    `;
    return commentHtml;
}
export function addComments() {
    //const commentForm = commentSection.querySelector('.comment__input__wrapp.form.flex');
    const commentInput = commentSection.querySelector('.comment__input');
    const btnComment = commentSection.querySelector('.comment__submit');
    
    const getLogin2 = JSON.parse(localStorage.getItem('login'));

    btnComment.addEventListener('click', () => {
        const headingText = document.querySelector('.task__desctiption.task__desctiption__card > .task__desctiption__header > .item__header.task > div > h2').textContent;
        console.log(headingText);
        const commentsObject = {
            name: getLogin2.name,
            team: getLogin2.team,
            comment: commentInput.value,
            date: pushTaskDate(),
            title: headingText,
        };
        
        const key = 'comments';
        const existingComments = JSON.parse(localStorage.getItem(key)) || [];
        existingComments.push(commentsObject);
        localStorage.setItem(key, JSON.stringify(existingComments));

        const commentHtml = `
        <div class="commet comment__block">
            <div class="commet__icon">
                <img src="img/userpic-big.jpg" alt="">
            </div>
            <div class="commet__text__wrapp">
                <p class="commet__name">${getLogin2.name}<span class="commet__role"> из команды «${getLogin2.team}»</span></p>
                <p class="commet__text">${commentInput.value}</p>
                <p class="commet__date">${pushTaskDate()}</p>
            </div>
        </div>
        `;

        commentHistorySection.insertAdjacentHTML('afterbegin', commentHtml);
        commentInput.value = null;
    });
}