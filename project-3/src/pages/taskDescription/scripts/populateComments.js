import { addComment } from "./comments";
const commentSection2 = document.querySelector('.task__desctiption__comments');
//const commentHistorySection2 = commentSection2.querySelector('.comment__history');
export function populateComments() {
    const headingText2 = document.querySelector('.task__desctiption.task__desctiption__card > .task__desctiption__header > .item__header.task > div > h2').textContent;
    const savedComments = JSON.parse(localStorage.getItem('comments'));

    if (savedComments) {
        savedComments.forEach(comment => {  
            if (comment.title === headingText2) {
                commentSection2.insertAdjacentHTML('beforeend', addComment(comment));
            }
        });
    }
}


