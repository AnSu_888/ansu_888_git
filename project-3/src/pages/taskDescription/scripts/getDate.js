export function pushTaskDate() {
    const now1 = new Date();
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric',
    };
    return now1.toLocaleString('en-US', options);
}

export function getTaskDate(string) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
    ];
    
    const now = new Date();
    const year = now.getFullYear();
    const month = monthNames[now.getMonth()];
    const date = now.getDate();
    
    if (string.includes(month) && string.includes(date) && string.includes(year)) {
        const today = 'today at' + string.slice(string.lastIndexOf(',') + 1);
        return today;
    }
    else {
      return `on ${string}`;
    }
}
