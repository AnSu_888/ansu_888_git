export function taskCreate(obj) {
    
    const taskHtml = `
    <a href="#" class="task task__block">
    <p class="task__title1"><b>${obj.title}</b></p>
    <p class="task__description">${obj.description}</p>
    <div class="flex">
        <img src="img/userpic.jpg" class="task__executor" alt="">
        <p class="task__label design">${obj.tag}</p>
    </div>
    <img src="img/img_prev.jpg" class="task__img" alt="">
    </a>
    `;
    return taskHtml;
}