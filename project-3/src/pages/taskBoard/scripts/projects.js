import { taskColumns } from "./appendTask";
import { populateFromLocalStorage, removeInitial } from "./populate";

function taskClickRedirect() {
    const taskBlocks = document.querySelectorAll('.task.task__block');
    taskBlocks.forEach(task => {
        task.addEventListener('click', () => {
            window.location.href = 'task-description.html';
        });
    });
}
export function projects() {
    const pjList = document.querySelectorAll('.projects__list > li > a');
    console.log(pjList);
    pjList.forEach(li => {
        if (li.textContent === document.querySelector('h1').textContent) {
            li.style.color = '#F3F4B2';
            window.addEventListener('load', () => {
                populateFromLocalStorage(taskColumns);
            });
        }

        li.addEventListener('click', () => {
            removeInitial(taskColumns);
            setTimeout(populateFromLocalStorage, 50, taskColumns);
            
            for (let count = 0; count < pjList.length; count++) {
                pjList[count].removeAttribute('style');
            }
            li.style.color = '#F3F4B2';
            document.querySelector('h1').textContent = li.textContent;

            setTimeout(taskClickRedirect, 100);
        });
    });
    setTimeout(taskClickRedirect, 100); 
}