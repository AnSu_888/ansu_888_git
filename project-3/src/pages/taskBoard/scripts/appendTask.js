import { taskCreate } from "./taskCreate";
import { pushTaskDate } from "../../taskDescription/scripts/getDate";
export const taskColumns = document.querySelectorAll('.task__column');

const btnAddTask = document.querySelectorAll('.item__btn');
const addTask = document.querySelector('.form--task');
const formAddTask = addTask.querySelector('form');
const inputs = formAddTask.querySelectorAll('input:not([type="submit"])');

const dropdown = addTask.querySelector('.tag__dropdown');
const plus = dropdown.querySelector('.dropdown__toggle > .info__icon');
const dropListItems = dropdown.querySelectorAll('.dropdown__list__item');

const dropdownExec = addTask.querySelector('.dropdown.w-260.gap--bottom');
const arrow = dropdownExec.querySelector('.info__icon');
const execListItems = dropdownExec.querySelectorAll('.dropdown__list__item');

const taskTitle = addTask.querySelector('#taskTitle');
const taskDescription = addTask.querySelector('#taskDescription');
const taskDeadline = addTask.querySelector('#deadline');
const taskObj = {};

export function taskAppend() {
    let divAppend;
    btnAddTask.forEach(btn => {
        btn.addEventListener('click', () => {
            taskColumns.forEach(column => {
                column.style.display = 'none';
            });
        addTask.style.display = 'block';

        
        divAppend = btn.closest('div').parentNode;
        taskObj.section = divAppend.querySelector('h2').textContent;
        });   
    });
    
    /////////////////////////DROPDOWNS
    plus.addEventListener('click', () => {
        dropdown.classList.toggle('dropdown--open');
    });
    
    dropListItems.forEach(item => {
        const defaultVal = plus.closest('div').querySelector('p');
            defaultVal.classList.remove('label');
            defaultVal.textContent = null;
            
        item.addEventListener('click', () => {
            const tag = item.querySelector('p').textContent;
            defaultVal.classList.add('label');
            defaultVal.textContent = tag;
            taskObj.tag = tag;
            dropdown.classList.remove('dropdown--open');
        });
    });
    
    arrow.addEventListener('click', () => {
        dropdownExec.classList.toggle('dropdown--open');
    });
    execListItems.forEach(item => {
        item.addEventListener('click', () => {
            const contractor = item.querySelector('p').textContent;
            const defaultVal = arrow.closest('div').querySelector('input');
            defaultVal.value = contractor;
            taskObj.contractor = contractor;
            dropdownExec.classList.remove('dropdown--open');
        });
    });
    ////////////////////////////////////////////////
    
    formAddTask.addEventListener('submit', (e) => {
        
        taskObj.title = taskTitle.value;
        taskObj.description = taskDescription.value;
        taskObj.deadline = taskDeadline.value;
        taskObj.project = document.querySelector('h1').textContent;
    
        taskObj.date = pushTaskDate();
        //console.log(taskObj);
    
        const key = 'tasks';
        const existing = JSON.parse(localStorage.getItem(key)) || [];
        existing.push(taskObj);
        localStorage.setItem(key, JSON.stringify(existing));
    
        taskColumns.forEach(column => {
            column.style.display = 'block';
        });
    
        addTask.style.display = 'none';
    
        divAppend.insertAdjacentHTML('beforeend', taskCreate(taskObj));

        inputs.forEach(input => {
            console.log(input.value);
            input.value = null;
            });
            taskDescription.value = null;
            const defaultVal2 = plus.closest('div').querySelector('p');
            defaultVal2.classList.remove('label');
            defaultVal2.textContent = null;
    });
}