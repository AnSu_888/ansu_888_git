import '../../../css/style.scss';
import { removeInitial } from './populate';
import { projects } from './projects';
import { taskColumns, taskAppend } from './appendTask';


const navItems = document.querySelectorAll('.tab__navigation > li');

removeInitial(taskColumns);
projects();
taskAppend();


navItems[1].addEventListener('click', () => {
    window.location.href = 'task-description.html';
});
navItems[2].addEventListener('click', () => {
    window.location.href = 'calendar.html';
});




