import { taskCreate } from "./taskCreate";

export function removeInitial(columns) {
    columns.forEach(column => {
        const initialToRemove = column.querySelectorAll('a:not(.item__btn)');
        initialToRemove.forEach(a => {
            a.remove();
        });
    });
}

export function populateFromLocalStorage(columns) {
    const saved = JSON.parse(localStorage.getItem('tasks'));
    if (saved) {
        saved.forEach(object => {      
            columns.forEach(column => {
                const title = column.querySelector('.item__title').textContent;
                const pjHeading = document.querySelector('h1').textContent;
                if (object.section === title && object.project === pjHeading) {
                    column.insertAdjacentHTML('beforeend', taskCreate(object));
                }
            });     
        }); 
    }
}