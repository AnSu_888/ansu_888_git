export function errorDivAdd(inputField, text, className) {
    function errorDivCreate() {
        const div = document.createElement('div');
        div.textContent = text;
        div.style.color = 'red';
        div.classList.add(className);
        return div;
    }
    inputField.style.border = "1px solid red";
    inputField.insertAdjacentElement('beforebegin', errorDivCreate());
}

export function errorDivRemove(parent, className, inputField) {
        if (parent.querySelector(`.${className}`)) {
            parent.removeChild(parent.querySelector(`.${className}`));
            inputField.removeAttribute('style');
        }    
    }

export function checkUserExistence(array, field) {
    function exists(object) {
        return object.email === field.value;  
    }
    if (array.some(exists)) {
        errorDivAdd(field, 'Пользователь с этим адресом уже существует.', 'userExists');
        throw new Error('User exists');   
    }
}

export function validateName(field) {
    const splitted = field.value.split(' ');
    if (splitted.length !== 2 || splitted[0].length < 3 || splitted[1].length < 3) {
        errorDivAdd(field, 'Имя и фамилия должны состоять минимум из 3 букв.', 'invalidName');
        throw new Error('Invalid name');   
    }
}
export function validateEmail(field) {
    const pattern = /^[A-Za-z0-9]+([\.-]?[A-Za-z0-9]+)*@[A-Za-z0-9]+([\.-]?[A-Za-z0-9]+)*(\.\w{2,3})+$/.test(field.value);
    if (!pattern) {
    errorDivAdd(field, 'Недопустимый формат адреса электронной почты.', 'invalidEmail');
        throw new Error('Invalid email'); 
    }
}