/**
 * Задача 5.
 *
 * Напишите функцию createLogger, которая возвращает объект с двумя методами: call и print.
 *
 * Метод call обладает одним обязательным параметром с типом function.
 * Вызов метода call вызывает функцию, переданную методу call в качестве первого аргумента.
 * Все остальные аргументы метода call, кроме первого (который функция),
 * передаются в качестве аргументов этой-же функции (из первого аргумента).
 *
 * Результат каждого вызова функции из первого аргумента метода call нужно сохранять.
 * Вызовы должны сохранятся в массиве.
 *
 * Каждый вызов должен быть объектом с тремя обязательными свойствами:
 * - name — это название функции;
 * - in — хранит массив входящих параметров;
 * - out — хранит то, что функция возвращает.
 *
 * Метод print возвращает массив с историей вызовов функций из первого аргумента метода `call`.
 *
 * Условия:
 * - Если метод call кроме коллбека в первом аргументе не принимает больше ничего, то в свойство in нужно записать пустой массив;
 * - Если функция ничего не возвращает то в out записывается undefined.
 *
 * Генерировать ошибку, если:
 * - В качестве первого аргумента методу call была передана не функция.
 */

// РЕШЕНИЕ

const arr = [];
function createLogger() {
    return {
        call(func, ...args) {
            if (typeof(func) === 'function') {
                arr.push({
                    name: func.name,
                    in: [...args],
                    out: func(...args)
                    }   
                );
                return func(...args); 
            }
            else {
                throw new Error('Not a function');
            }    
        },
        print() {
            return arr;
        }    
    }
}
////////////////////////////////Inner functions
const sum = (...rest) => {
    let sum = 0;
    rest.forEach(item => {
        sum += item; 
    });
    return sum;
};
const returnIdentity = n => n;
const returnNothing = () => undefined;
//////////////////////////////////////////////////////////////////

const logger1 = createLogger();
logger1.call(sum, 1, 3, 4, 10);
logger1.call(returnIdentity, 1);
logger1.call(returnNothing);
console.log(logger1.print());

const logger2 = createLogger();
logger2.call(sum, 3, 4); 
logger2.call(returnIdentity, 9);
logger2.call(returnNothing); 
console.log(logger2.print()); 



// const arr = [];
// function createLogger(func, ...args) {
//     arr.push({
//         name: func.name,
//         in: [...args],
//         out: func(...args)
//         }   
//     );
//     return func(...args); 
// }

// function print() {
//     return arr;
// }

// ////////////////////////////////Inner functions
// const sum = (...rest) => {
//     let sum = 0;
//     rest.forEach(item => {
//         sum += item; 
//     });
//     return sum;
// };
// const returnIdentity = n => n;
// const returnNothing = () => undefined;
// //////////////////////////////////////////////////////////////////

// console.log(createLogger(sum, 1, 3));
// createLogger(sum, 4, 3);
// createLogger(sum, 10, 3);
// createLogger(returnIdentity, 9);
// createLogger(returnNothing);
// console.log(print());



