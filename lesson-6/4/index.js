/**
 * Задача 4.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 * 
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

// РЕШЕНИЕ

function getDivisors(num) {
    const arr = []; 
    if (typeof(num) !== 'number')
    {
        throw new Error('It should be a number.');
    }
    else if (num === 0) {
        throw new Error('It can\'t be zero.');
    }
    else {
        for (let i = 1; i <= num; i++) {
            if (num % i === 0) {
                arr.push(i);
            }
        }
        return arr;
    }
}
console.log(getDivisors(12));


