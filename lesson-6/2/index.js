/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f(...rest) {
    let sum = 0;
    rest.forEach(item => {
        if (typeof(item) === 'number') {
            sum += item;
        }
        else {
            throw new Error('Not a number');
        }  
    });
    return sum;
}

// function f() {
//     let sum = 0;
//     for (let i = 0; i < arguments.length; i++) {
//         if (typeof(arguments[i]) === 'number') {
//             sum += arguments[i];
//         }
//         else {
//             throw new Error('Not a number');
//         } 
//     }
//     return sum;
// }


// function f(...rest) {
//     let sum = 0;
//     for (const item of rest) {
//         if (typeof(item) === 'number') {
//             sum += item;
//         }
//         else {
//             throw new Error('Not a number');
//         }
//     }
//     return sum;
// }



console.log(f(1, 1, 1, 2, 1, 1, 1, 1, 1)); 
console.log(f(1, 1, 1)); 
console.log(f(1, 1, 5, 98)); 
