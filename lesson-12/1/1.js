/**
 * Примерные данные для заполнения, вы можете использовать свои данные.
 *  Имя: "Firefox",   Компания: "Mozilla",        Процент: "8.01%"        
 *  Имя: "Chrome",    Компания: "Google",         Процент: "68.26%"
 *  Имя: "Edge",      Компания: "Microsoft",      Процент: "6.67%"        
 *  Имя: "Opera",     Компания: "Opera Software", Процент: "1.31%"
 * 
 */
const arr = [];
const form = document.querySelector('#form');

const textInputs = form.querySelectorAll('input[type="text"]');
const inputBrowser = form.querySelector('input[name="browser"]');
const inputCompany = form.querySelector('input[name="company"]');
const inputPercent = form.querySelector('input[name="percent"]');

const btnAdd = form.querySelector('input[type="submit"]');
const btnAnalize = document.querySelector('button');
btnAdd.setAttribute('disabled', true);

function validationStr(item) {
    if (item.value.length < 3 || !isNaN(item.value)) {
        item.classList.add('error');
    } 
    else {
        item.classList.remove('error'); 
    }
}
function validationNum(item) {
    if (isNaN(item.value) || !item.value.length || item.value < 0) {
        item.classList.add('error');
    } 
    else {
        item.classList.remove('error'); 
    }
}

textInputs.forEach(input => {
    input.addEventListener('input', () => {
        validationStr(inputBrowser);
        validationStr(inputCompany);
        validationNum(inputPercent);
  
        if (   !inputCompany.classList.contains('error') 
            && !inputBrowser.classList.contains('error')
            && !inputPercent.classList.contains('error')) {
            btnAdd.removeAttribute('disabled');
        }
        else {
            btnAdd.setAttribute('disabled', true);   
        } 
    });
});

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const browser = formData.get('browser');
    const company = formData.get('company');
    const percent = formData.get('percent');
     arr.push({
        name: browser,
        company: company,
        marketShare: percent,
     });
    console.log(arr); 
    form.reset();
});

btnAnalize.addEventListener('click', () => {
    try {
        if (!arr.length) {
            alert('Недостаточно данных'); 
        }
        else {
            let winnerPercent = 0;
            let winnerCompany = null;
            let winnerBrowser = null;
    
            arr.forEach(object => {
                const {name, company, marketShare} = object; 
                const correctSort = [marketShare, winnerPercent].sort(function(a, b) {
                        return a - b;
                    });
                    if (correctSort[1] === marketShare) {
                        winnerPercent = marketShare;
                        winnerCompany = company;
                        winnerBrowser = name;
                    }
            }); 
            console.log(`Самый востребованный браузер — это ${winnerBrowser} от компании ${winnerCompany} с процентом использования ${winnerPercent}%.`);
        }
    }
    catch(e) {
        console.log('Ошибка обработки данных');
    }   
});
