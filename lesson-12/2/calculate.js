import {arr} from './array.js';

function calculate() {
    let winnerPercent = 0;
    let winnerCompany = null;
    let winnerBrowser = null;

    arr.forEach(object => {
        const {name, company, marketShare} = object; 
        const correctSort = [marketShare, winnerPercent].sort(function(a, b) {
                return a - b;
            });
            if (correctSort[1] === marketShare) {
                winnerPercent = marketShare;
                winnerCompany = company;
                winnerBrowser = name;
            }
    }); 
    console.log(`Самый востребованный браузер — это ${winnerBrowser} от компании ${winnerCompany} с процентом использования ${winnerPercent}%.`);
}

export {calculate};