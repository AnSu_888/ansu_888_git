import { payments, paymentObjects } from "./form";
import { formRightCheckboxes, transList } from "./appendTransactions";

export const payList = document.querySelector('.form__summary-list');
export const listTotal = document.querySelector('.list__total > p > .price > b');
export const meters = document.querySelector('#meters');
       const emptyOption = document.createElement('option');
       meters.insertAdjacentElement('afterbegin', emptyOption);
const btnClear = document.querySelector('.btn-outline');

export const previous = document.querySelector('input[id="previous"]');
previous.setAttribute('pattern', '[0-9.]{0,12}');
export const current = document.querySelector('input[id="current"]');
current.setAttribute('pattern', '[0-9.]{0,12}');

export function clearList() {
    const lis = payList.querySelectorAll('.list__item:not(.list__total)');
    listTotal.textContent = 0;
    lis.forEach(li => {
        li.remove();
    });
    formRightCheckboxes.forEach(item => {
        item.closest('label').querySelector('input').checked = false;
    });
}
export function clearInputs() {
    meters.selectedIndex = 0;
    previous.value = null;
    current.value = null;
}

export function clearLocalStorage() {
    if (!transList.querySelector('.list__item-error')) {
        localStorage.clear();
        clearInputs();
        clearList();
    }
}

export function clearAll() {
    btnClear.addEventListener('click', () => {
        clearList();
        payments.splice(0, payments.length);
        paymentObjects.splice(0, paymentObjects.length);
        localStorage.clear();
        transList.innerHTML = '';
    });
}

