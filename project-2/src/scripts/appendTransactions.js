import { clearLocalStorage } from "./clear";

let balance = 100;
const formRight = document.querySelector('.right__payments');
export const formRightCheckboxes = formRight.querySelectorAll('span');
const rightTransactions = document.querySelector('.right__transactions');
export const transList = rightTransactions.querySelector('ul');
transList.innerHTML = '';
  
function appendRightList(classes, message) {
    const rightLiItem = document.createElement('li');
    rightLiItem.classList.add(...classes);
    rightLiItem.textContent = message; 
    
    transList.insertAdjacentElement('beforeend', rightLiItem);  
}

export function appendTransactions(arrayOfObjects) {
    formRight.addEventListener('submit', (event) => {
        event.preventDefault();
        formRightCheckboxes.forEach(item => {
            if (item.closest('label').querySelector('input').checked) {
                arrayOfObjects = JSON.parse(localStorage.getItem('payments'));//???????????????
                arrayOfObjects.forEach(object => {
                    if (object.name === item.textContent) {
                        balance -= object.payable;
                        if (balance >= 0) {
                            appendRightList(['list__item'], `${object.name}: успешно оплачено`);
                        }
                        else {
                            appendRightList(['list__item', 'list__item-error'], `${object.name}: ошибка транзакции`);
                        }  
                    }  
                });
            }
        });
        clearLocalStorage();
    });
}






