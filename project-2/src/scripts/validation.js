function errorDivCreate(text, className) {
    const div = document.createElement('div');
    div.textContent = text;
    div.style.color = 'red';
    div.classList.add(className);
    return div;
}
const div2 = errorDivCreate('Текущее значение не может быть меньше предыдущего.', 'validationDiv');
const div1 = errorDivCreate('Недопустимое значение.', 'validationZero');
div2;
div1;

function errorDivAdd(elem, inputField) {
    inputField.style.border = "1px solid red";
    inputField.closest('div').appendChild(elem);
}
function errorDivRemove(elem, inputField) {
    inputField.closest('div').removeChild(elem);
    inputField.removeAttribute('style');
}

// export function returnEmpty(property, div) {
//         if (!property) {
//             div.style.border = "1px solid red";
//             return false;   
//         }
// }

export function validationNum(item, button) {
    if (/[^\d.]/.test(item.value)) {
        item.value = item.value.replace(/[^\d.]/, '');
    }
    if (/^0{2}/.test(item.value)) {
        item.value = item.value.replace(/^0{2}/, '0');
    }
    if (/^0\d/.test(item.value)) {
        item.value = item.value.replace(/^0/, '');
    }

    item.addEventListener('change', () => {
        if (item.value === '0' || item.value === '0.') {
            errorDivAdd(div1, item);
            button.setAttribute('disabled', true);
            button.style.cssText = 'background-color: rgba(19, 1, 1, 0.1); color: rgba(16, 16, 16, 0.3); border-color: rgba(16, 16, 16, 0.3)';
        }
        else if (item.closest('div').querySelector('.validationZero')) {
                errorDivRemove(div1, item); 
                button.removeAttribute('disabled');
                button.style.cssText = 'none';
        }
    });
}

export function compareInputs(button) {
    if (+current.value < +previous.value) {
        errorDivAdd(div2, current);
        button.setAttribute('disabled', true);
        button.style.cssText = 'background-color: rgba(19, 1, 1, 0.1); color: rgba(16, 16, 16, 0.3); border-color: rgba(16, 16, 16, 0.3)';       
    }
    else if (current.closest('div').querySelector('.validationDiv')) {
        errorDivRemove(div2, current);
        button.removeAttribute('disabled');
        button.style.cssText = 'none';
    }   
}



