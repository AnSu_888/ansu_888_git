import '../styles/index.scss';
import { appendTransactions } from './appendTransactions';
import { clearInputs, clearList, clearAll } from './clear';
import { form1, populateFromLocalStorage, saved } from './form';
import { paymentCreate } from './paymentCreate';

populateFromLocalStorage();

clearList();
clearInputs();
clearAll();

paymentCreate();
form1();
appendTransactions(saved);





