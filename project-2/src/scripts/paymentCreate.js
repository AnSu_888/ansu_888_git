
import { services, payment, form } from "./form";
import { current, previous, meters } from "./clear";
import { compareInputs, validationNum } from "./validation";

export const centerTitle = document.querySelector('.center__title');
export const centerDesc = document.querySelector('.center__desc');
export const saveBtn = form.querySelector('.btn');
export function paymentCreate() {
    services.forEach(service => {
        const objDescr = {
            'Налоги': 'Оплата налогов',
            'Холодная вода': 'Оплата холодного водоснабжения',
            'Интернет': 'Оплата Интернета',
            'Охрана': 'Оплата охраны',
            'Обмен валют': 'Операции по обмену валют',
        };
        if (service.classList.contains('selected')) {
            payment.id = service.getAttribute('data-id');
        }
        
        payment.name = centerTitle.textContent;
        service.addEventListener('click', () => {
            const getHeading = service.querySelector('p').textContent;
            centerTitle.textContent = getHeading;
            centerDesc.textContent = objDescr[getHeading];
            
            for (let count = 0; count < services.length; count++) {
                services[count].classList.remove('selected');
            }
            service.classList.add('selected');
            payment.id = service.getAttribute('data-id');
            payment.name = getHeading;
        });
    });
    
    meters.addEventListener('change', (event) => {
        if (meters.hasAttribute('style')) {
            meters.removeAttribute('style');
        }
        payment.meterId = event.target.options[meters.selectedIndex].text;
    });
    
    previous.addEventListener('input', () => {
        validationNum(previous, saveBtn);
        compareInputs(saveBtn);
        payment.previous = +previous.value;
    });
    
    current.addEventListener('input', () => {
        validationNum(current, saveBtn);
        compareInputs(saveBtn);
        payment.current = +current.value;
    });
}
