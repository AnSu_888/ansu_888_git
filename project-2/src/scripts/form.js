import { tarifs } from './constants';
import { meters, clearInputs, payList, listTotal, previous } from './clear';
import { formRightCheckboxes } from "./appendTransactions";

export const services = document.querySelectorAll('[data-id]');
export const payments = [];
export const paymentObjects = [];
export const payment = {};
export const form = document.querySelector('.center__form');

function checkCheckbox(name) {
    formRightCheckboxes.forEach(checkbox => {
        if (checkbox.textContent === name) {
            checkbox.closest('label').querySelector('input').checked = true;
        }
    });
}

function populateSummary(meter, money, array) {
    const listElem = 
    `<li class="list__item">
        <p>
            <span class="list__item-label">${meter}</span>
            <span class="price">$ <b>${money}</b></span>
        </p>
    </li>`;

    payList.insertAdjacentHTML('afterbegin', listElem);
    listTotal.textContent = array.reduce((sum, current) => 
        (+sum + +current).toFixed(1));
}
export let saved = JSON.parse(localStorage.getItem('payments'));
export function populateFromLocalStorage() {
    window.addEventListener('load', () => {
        const payableArray = JSON.parse(localStorage.getItem('payable'));
        if (saved) {
            saved.forEach(object => {
                checkCheckbox(object.name);
                populateSummary(object.meterId, object.payable, payableArray);
            }); 
        }
    });
}

export function form1() {
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        // function empty(property, div) {
        //     return returnEmpty(property, div);
        // }
       // empty(payment.previous, previous);
        
        if (!payment.meterId) {
            meters.style.border = "1px solid red";
            return false;
        }
        if (!payment.previous) {
            previous.style.border = "1px solid red";
            return false;
        }
    /////////////////////////////////////////
            const payable = ((payment.current - payment.previous) * tarifs[payment.id]).toFixed(1);
            payments.push(payable);
            payment.payable = payable;
        
            const clone = Object.assign({}, payment);
            paymentObjects.push(clone);
    
            console.log(paymentObjects);
            console.log(payments);
            
            const lastPayment = paymentObjects[paymentObjects.length - 1];
            populateSummary(lastPayment.meterId, lastPayment.payable, payments);
            checkCheckbox(lastPayment.name);

     ///////////////////////////////PAYMENT CLEAR
        for (const key in payment) {
            delete payment[key];
        }
        clearInputs();
        services.forEach(service => {
            if (service.classList.contains('selected')) {
                payment.id = service.getAttribute('data-id');
            }
        });
    //////////////////////////////////////////////////
    localStorage.setItem('payments', JSON.stringify(paymentObjects));
    localStorage.setItem('payable', JSON.stringify(payments));
    });
}