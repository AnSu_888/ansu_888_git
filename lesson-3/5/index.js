/**
 * Задание 5
 * 
 * Используя цикл найти факториал числа.
 * Факториал числа вывести в консоль.
 */

const number = prompt('Введите число:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

//РЕШЕНИЕ

let enteredNum = +number;
let res;
if (enteredNum === 0) {
    res = 0;
    console.log(res);
    
} else if (isNaN(enteredNum)) {
        console.log('Please enter a number.');
} else if (!Number.isInteger(enteredNum)) {
    console.log('Please enter an integer number.');

} else if (enteredNum > 170) {
    console.log('Too large number.');

} else if (enteredNum < 0) {
    console.log('Please enter a positive number.');
    
} else {
    res = 1;
    for (i = 1; i < enteredNum; i++)
    {
        res *= i + 1;
    }
    console.log(res);
}

