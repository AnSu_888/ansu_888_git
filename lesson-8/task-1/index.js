/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ

const formGr1 = {
    className: ['form-group'],
    label: {
        for: 'email',
        textContent: 'Электропочта'
    },
    input: {
        name: 'email',
        type: 'email',
        class: 'form-control',
        id: 'email',
        placeholder: 'Введите свою электропочту'
    }
};
const formGr2 = {
    className: ['form-group'],
    label: {
        for: 'password',
        textContent: 'Пароль'
    },
    input: {
        name: 'password',
        type: 'password',
        class: 'form-control',
        id: 'password',
        placeholder: 'Введите пароль'
    }
};
const formGr3 = {
    className: ['form-group', 'form-check'], 
    label: {
        for: 'exampleCheck1',
        textContent: 'Запомнить меня',
        class: 'form-check-label'
    },
    input: {
        name: 'checkbox',
        type: 'checkbox',
        class: 'form-check-input',
        id: 'exampleCheck1'
    }
};

const form = document.querySelector('#form');

function divCreate(objName) {
    const div = document.createElement('div');
    form.appendChild(div);
    for (i = 0; i < objName.className.length; i++) {
        div.classList.add(objName.className[i]);
    }
    
    const label = document.createElement('label');
    div.appendChild(label);
    
    label.setAttribute('for', objName.label.for);
    label.textContent = objName.label.textContent;
    
    const input = document.createElement('input');
    div.appendChild(input);
    
    for (const key in objName.input) {
        input.setAttribute(key, objName.input[key]);
    }
    if (objName.className.includes('form-check')) {
        const lastDiv = document.querySelector('.form-check');
        lastDiv.insertBefore([...lastDiv.children][1], [...lastDiv.children][0]);
        label.setAttribute('class', objName.label.class);
    }
}
function buttonCreate() {
    const button = document.createElement('button');
    form.appendChild(button);
    
    button.setAttribute('type', 'submit');
    button.classList.add('btn', 'btn-primary');
    button.textContent = 'Вход';
}

divCreate(formGr1);
divCreate(formGr2);
divCreate(formGr3);
buttonCreate();













