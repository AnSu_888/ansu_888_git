/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

const formGr1 = {
    className: ['form-group'],
    label: {
        for: 'email',
        textContent: 'Электропочта'
    },
    input: {
        name: 'email',
        type: 'email',
        class: 'form-control',
        id: 'email',
        placeholder: 'Введите свою электропочту'
    }
};
const formGr2 = {
    className: ['form-group'],
    label: {
        for: 'password',
        textContent: 'Пароль'
    },
    input: {
        name: 'password',
        type: 'password',
        class: 'form-control',
        id: 'password',
        placeholder: 'Введите пароль'
    }
};
const formGr3 = {
    className: ['form-group', 'form-check'], 
    label: {
        for: 'exampleCheck1',
        textContent: 'Запомнить меня',
        class: 'form-check-label'
    },
    input: {
        name: 'checkbox',
        type: 'checkbox',
        class: 'form-check-input',
        id: 'exampleCheck1'
    }
};

const form = document.querySelector('#form');

function divCreate(objName) {
    const div = document.createElement('div');
    form.appendChild(div);
    for (i = 0; i < objName.className.length; i++) {
        div.classList.add(objName.className[i]);
    }
    
    const label = document.createElement('label');
    div.appendChild(label);
    
    label.setAttribute('for', objName.label.for);
    label.textContent = objName.label.textContent;
    
    const input = document.createElement('input');
    div.appendChild(input);
    
    for (const key in objName.input) {
        input.setAttribute(key, objName.input[key]);
    }
    if (objName.className.includes('form-check')) {
        const lastDiv = document.querySelector('.form-check');
        lastDiv.insertBefore([...lastDiv.children][1], [...lastDiv.children][0]);
        label.setAttribute('class', objName.label.class);
    }
}
function buttonCreate() {
    const button = document.createElement('button');
    form.appendChild(button);
    
    button.setAttribute('type', 'submit');
    button.classList.add('btn', 'btn-primary');
    button.textContent = 'Вход';
}

divCreate(formGr1);
divCreate(formGr2);
divCreate(formGr3);
buttonCreate();

////////////////////////EVENTS//////////////////////
form.addEventListener('submit', (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const email = formData.get('email');
    const password = formData.get('password');
    if (password.trim().length === 0) {
        throw new Error('Empty value!'); 
    }
    const remember = document.querySelector('#exampleCheck1').checked;
    //const remember2 = formData.get('checkbox');
   
    const key = 'Object';
    localStorage.setItem(key, JSON.stringify({email, password, remember}));
});


