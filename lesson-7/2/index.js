/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true, 
 * иначе - false. 
 * 
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 * 
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
*/

// const arr1 = [1, 2, 3];
// const arr2 = [1, 2, 3];
// let flag = '';

// РЕШЕНИЕ
function errorThrow(arr1, arr2) {
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) {
        throw new Error('Not an array!'); 
    }
}

function arrayCompare(array1, array2) {
    errorThrow(array1, array2);
    if (array1.length === array2.length) {
    const flag = array1.every((elem, i) => {
            return elem === array2[i];
    });
    return flag; 
} 
    else {
        return false;
    }     
}

console.log(
    arrayCompare([], [1, 2, 3])
);





            
                
            