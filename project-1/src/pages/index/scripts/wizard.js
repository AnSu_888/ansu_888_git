import { saveToStorage } from './login';
/////BLOCKS
const loginBlock = document.querySelector('#loginBlock');
const step1Block = document.querySelector('#step1Block');
const regBlock = document.querySelector('#regBlock');
/////BUTTONS
const regBtn = document.querySelector('#regBtn');
const toStep2Btn = document.querySelector('#toStep2Btn');
const toLoginSvg = document.querySelector('#toLoginSvg');
const from3to2Svg = document.querySelector('#from3to2Svg');
const createAccount = document.querySelector('#createAccount');
/////FORM
const formLogin = regBlock.querySelector('.form-login');
const password = document.querySelector('#password');
const passwordNext = document.querySelector('#password_next');
const name = document.querySelector('#name');
const email = document.querySelector('#email');

const userObject = {};
const userRole = step1Block.querySelectorAll('input[type="radio"]');
userRole[0].dataset.role = 'student';
userRole[1].dataset.role = 'teacher';

function blockSwitch(btn, noneBlock, flexBlock) {
   btn.addEventListener('click', () => {
        noneBlock.style.display = 'none';
        flexBlock.style.display = 'flex';
       
        userRole.forEach(input => {
            if (input.checked) {
                userObject.type = input.getAttribute('data-role');
            }
        });
    });
}

blockSwitch(regBtn, loginBlock, step1Block);
blockSwitch(toStep2Btn, step1Block, regBlock);
blockSwitch(toLoginSvg, step1Block, loginBlock);
blockSwitch(from3to2Svg, regBlock, step1Block);

//  function validationCheck(keyName, keyPass, keyPass2) {
//     if (keyPass.value !== keyPass2.value) {
//         keyPass.classList.add('error');
//         keyPass2.classList.add('error');
//         return false;    
//         }
//         const splitted = keyName.value.split(' ');
//         if (splitted.length !== 2 || splitted[0].length < 3 || splitted[1].length < 3) {
//             keyName.classList.add('error');
//             return false;
//         }
//  }
createAccount.addEventListener('click', () => {
    //validationCheck(name, password, passwordNext);
    if (password.value !== passwordNext.value) {
    password.classList.add('error');
    passwordNext.classList.add('error');
    return false;    
    }
    const splitted = name.value.split(' ');
    if (splitted.length !== 2 || splitted[0].length < 3 || splitted[1].length < 3) {
        name.classList.add('error');
        return false;
    }

    userObject.name = name.value;
    userObject.email = email.value;
    userObject.password = password.value;

    if (userObject.type === 'teacher') {
        const key = 'teacher';
        localStorage.setItem(key, JSON.stringify(userObject));
    }
    else if ((userObject.type === 'student')) {
        const key = 'students';
        const existing = JSON.parse(localStorage.getItem(key)) || [];
        existing.push(userObject);
        localStorage.setItem(key, JSON.stringify(existing));
    }
    localStorage.setItem('login', JSON.stringify(userObject));

    formLogin.reset();
    formLogin.submit();

    if (userObject.type === 'teacher') {
        window.location.href = 'teacher.html';
    } else if (userObject.type === 'student') {
        window.location.href = 'student.html';
    }
});





