import { timeSlots } from './constants';
import { lessons } from './constants';


    const timeObject = {};
    const form = document.querySelector('#formLessons');
    
    function todayTomorrow(radio) {
        const p = radio.closest('.time-slot-row').querySelector('p').textContent;
        return p === 'Завтра';   
    }
    
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const timeRadio = form.querySelector('input[name="time"]:checked');
        const typeRadio = form.querySelector('input[name="type"]:checked');
    
        const getUser = JSON.parse(localStorage.getItem('login'));
        timeObject.name = getUser.name;
        timeObject.time = timeSlots[timeRadio.id];
        timeObject.tomorrow = todayTomorrow(timeRadio);
        timeObject.title = lessons[typeRadio.id].title;
        timeObject.duration = lessons[typeRadio.id].duration;
        
        const key = 'lessons';
        const existing = JSON.parse(localStorage.getItem(key)) || [];
        existing.push(timeObject);
        
        localStorage.setItem(key, JSON.stringify(existing));  
    });

