function timeConvert(duration, bookedTime) {
    const hours = (duration / 60);
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;
    const rminutes = Math.round(minutes);
    const hoursSum = bookedTime + rhours;
    if (rminutes !== 0) {
        return `${bookedTime}:00–${hoursSum}:${rminutes}`;
    }
    else {
        return `${bookedTime}:00–${hoursSum}:${rminutes}0`;
    }  
}

function day(tomorrowValue) {
    if (tomorrowValue === true) {
        return 'Завтра';
    }
    if (tomorrowValue === false) {
        return 'Сегодня';
    }
}

const cardParent = document.querySelector('.block__scheduled-lessons');
const oldDivs = cardParent.querySelectorAll('.card-box');

    const bookedSlot = JSON.parse(localStorage.getItem('lessons'));
    bookedSlot.forEach(object => {
        const cardName = object.name;
        const cardTitle = object.title;
        const cardTime = timeConvert(object.duration, object.time);
        const cardDay = day(object.tomorrow);
        //console.log(cardName, cardDay, cardTime, cardTitle);

        const card =
        `<div class="card-box">
            <div class="card-illustration">
                <img src="./images/user_03.png" alt="">
            </div>
            <div class="info">
                <p class="sub-title">${cardDay}, ${cardTime}</p>
                <p class="info-title">${cardName}</p>
                <p class="info-desc">${cardTitle}</p>
            </div>
        </div>`;

        oldDivs.forEach(div => {
            div.remove();
        });
        cardParent.insertAdjacentHTML('beforeend', card);
    });

    
        
    



    